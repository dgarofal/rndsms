package com.example.mitsest.smsrnd.utils;

import android.app.Activity;
import android.content.pm.PackageManager;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

public class PermissionDelegate {
    @NonNull private List<PermissionAction> permissionActions;
    @NonNull private WeakReference<Activity> activityWeakReference;

    public PermissionDelegate(@NonNull Activity activity) {
        this.permissionActions = new ArrayList<>();
        activityWeakReference = new WeakReference<>(activity);
    }

    public void executeActionWithPermission(@NonNull PermissionAction permissionAction) {
        final Activity activity = weakReferenceGet(activityWeakReference);

        if (activity == null || permissionAction.getRequestedPermissions() == null) {
            return;
        }

        if (isPermissionGrantedForAction(permissionAction)) {
            permissionAction.onPermissionGranted();
        } else {
            permissionActions.add(permissionAction);
            ActivityCompat.requestPermissions(activity, permissionAction.getRequestedPermissions(), permissionAction.getPermissionRequestCode());
        }
    }

    public boolean isPermissionGrantedForAction(PermissionAction permissionAction) {
        final Activity activity = weakReferenceGet(activityWeakReference);
        if (activity == null) {
            return false;
        }

        for (int i = 0; i < permissionAction.getRequestedPermissions().length; i++) {
            if (ActivityCompat.checkSelfPermission(activity, permissionAction.getRequestedPermissions()[i]) != PackageManager.PERMISSION_GRANTED) {
                return false;
            }
        }

        return true;
    }

    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {
        PermissionAction foundPermissionAction = null;
        for (PermissionAction permissionAction : permissionActions) {
            if (requestCode == permissionAction.getPermissionRequestCode()) {
                foundPermissionAction = permissionAction;
                break;
            }
        }
        if (foundPermissionAction != null) {

            final Activity activity = weakReferenceGet(activityWeakReference);
            if (activity == null) {
                return;
            }

            permissionActions.remove(foundPermissionAction);
            if (isPermissionGrantedForAction(foundPermissionAction)) {
                foundPermissionAction.onPermissionGranted();
            } else {
                if (isMarshmallow()) {
                    for (String permission : permissions) {
                        if (!activity.shouldShowRequestPermissionRationale(permission)) {
                            foundPermissionAction.onPermissionNeverAskAgainChecked();
                        } else {
                            foundPermissionAction.onPermissionRejected();
                        }
                    }
                } else {
                    foundPermissionAction.onPermissionRejected();
                }
            }
        }
    }

    public static @Nullable <T> T weakReferenceGet(@Nullable WeakReference<T> weakReference) {
        if (weakReference == null || weakReference.isEnqueued()) {
            return null;
        }

        return weakReference.get();
    }

    public static boolean isMarshmallow() { return Build.VERSION.SDK_INT >= Build.VERSION_CODES.M; }
}
