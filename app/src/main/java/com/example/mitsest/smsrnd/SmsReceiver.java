package com.example.mitsest.smsrnd;

import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.telephony.SmsMessage;
import android.util.Log;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SmsReceiver extends BroadcastReceiver {

    public interface SmsListener {
        void messageReceived(@Nullable String messageText);
    }

    private static @Nullable SmsListener mListener;

    // Listening for sms messages containing a 6-digit number
    public Pattern p = Pattern.compile("(|^)\\d{6}");


    @SuppressLint("LogNotTimber")
    @Override
    public void onReceive(Context context, Intent intent) {

        if (mListener == null) {
            return;
        }

        Bundle data = intent.getExtras();

        if (data == null) {
            return;
        }

        Object[] pDus = (Object[]) data.get("pdus");
        String format = data.getString("format");

        if (pDus == null) {
            return;
        }

        for (Object pdu : pDus) {
            SmsMessage smsMessage;

            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
                smsMessage = SmsMessage.createFromPdu((byte[]) pdu, format);
            } else {
                smsMessage = SmsMessage.createFromPdu((byte[]) pdu);
            }

            if (smsMessage == null) {
                return;
            }

//            String sender = smsMessage.getDisplayOriginatingAddress();
//            String phoneNumber = smsMessage.getDisplayOriginatingAddress();
//            String senderNum = phoneNumber;

            String messageBody = smsMessage.getMessageBody();
            try {
                if (messageBody != null) {
                    Matcher m = p.matcher(messageBody);
                    if (m.find()) {
                        mListener.messageReceived(m.group(0));
                    }
                }
            } catch (Exception e) {
                Log.e("SmsReceiver", "", e);
            }
        }
    }

    public static void bindListener(@Nullable SmsListener listener) {
        mListener = listener;
    }
}
