package com.example.mitsest.smsrnd.utils;

public interface PermissionAction {
    void onPermissionGranted();

    void onPermissionRejected();

    void onPermissionNeverAskAgainChecked();

    String[] getRequestedPermissions();

    int getPermissionRequestCode();

}
