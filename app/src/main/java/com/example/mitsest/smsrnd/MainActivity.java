package com.example.mitsest.smsrnd;

import android.Manifest;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;

import com.example.mitsest.smsrnd.utils.PermissionAction;
import com.example.mitsest.smsrnd.utils.PermissionDelegate;

public class MainActivity extends AppCompatActivity implements PermissionAction, SmsReceiver.SmsListener {

    TextView codeTextView;

    /*
     * Permissions
     */
    @NonNull private final PermissionDelegate permissionDelegate;

    public MainActivity() {
        super();
        permissionDelegate = new PermissionDelegate(this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        codeTextView = findViewById(R.id.code);

        permissionDelegate.executeActionWithPermission(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        SmsReceiver.bindListener(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        SmsReceiver.bindListener(null);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {
        permissionDelegate.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }


    @Override
    public void onPermissionGranted() {
        Toast.makeText(this, "Hello!", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onPermissionRejected() {

    }

    @Override
    public void onPermissionNeverAskAgainChecked() {

    }

    @Override
    public String[] getRequestedPermissions() {
        return new String[]{Manifest.permission.RECEIVE_SMS, Manifest.permission.READ_SMS};
    }

    @Override
    public int getPermissionRequestCode() {
        return 9999;
    }

    @Override
    public void messageReceived(String messageText) {
        if (codeTextView != null && messageText != null) {
            codeTextView.setText(messageText);
        }
    }
}
